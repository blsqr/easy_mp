'''The Reporter class and some accompanying functions.'''

import os
import sys
import glob
import logging
import subprocess # to get folder size
from datetime import timedelta
from datetime import datetime as dati
from collections import defaultdict, OrderedDict

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.header import Header
from email import encoders
from email.utils import formataddr

import numpy as np

from .easy_mp import touch

# Set up a logger
log = logging.getLogger(__name__)

# For colored terminal commands
def no_color(s, *args, **kwargs):
	''' Just returns the argument s as a string -- used to substitue termcolor.colored if no colored output is desired.'''
	return str(s)
if sys.stdout.isatty():
	try:
		from termcolor import colored
	except ImportError:
		# not installed, use a different method, which does not do anything with the color argument
		colored = no_color
else:
	# just set to a do-nothing-function
	colored = no_color

# -----------------------------------------------------------------------------

class Reporter:
	''' This class is a Reporter. It can handle callbacks from functions and -- making some assumptions -- reports on these, e.g. to a file ...'''

	# Class constants
	POSSIBLE_STATUS = ['calls', 'success', 'skipped', 'aborted', 'failed']
	ETA_EXCLUDE		= {'calls', 'skipped', 'aborted', 'failed'} # which counters to not count into the number of calls from which ETA and other time-based stuff is computed

	# .........................................................................
	def __init__(self, filepath: str="_report", obs_dir: str="", stdout_only: bool=False, max_n: int=0, ring_bell: bool=True, term_reps: bool=False, status_files: bool=False, stepsize: int=1, skipping: list=None, eof: str="", save_callback: bool=True, add_status: list=None, mail_kwargs: dict=None):

		log.debug("Initialising Reporter object ...")

		# Handle kwargs and default values ....................................
		# Whether to write files or not
		self.stdout_only= stdout_only

		# The filepath to write the report file to
		self.filepath 	= filepath

		# The observation directory, e.g. where simulation output is saved
		self.obs_dir	= obs_dir

		# Number of calls to reach the end
		self.max_n  	= max_n

		# Whether to print \a at each call
		self.bell 		= ring_bell

		# One-line terminal reports?
		self.term_reps	= term_reps

		# By how much to increment counters
		self.stepsize 	= stepsize

		# Get the possible status the reporter handles
		self.statuses 	= self.POSSIBLE_STATUS
		if add_status:
			self.statuses 	+= add_status

		# Whether to check for status files in simulation directory, which can be used to show which status other simulations have...
		self.status_files = status_files

		# Skipping
		self.skipping 		= [] if skipping is None else skipping
		self.max_n    		-= len(self.skipping)

		# Whether to send an email at the last call
		if mail_kwargs is None:
			mail_kwargs = {}
		self.send_mail 	= mail_kwargs.pop('enabled', False)
		self.mail_kwargs= mail_kwargs

		# Initialize other attributes (no kwargs) .............................
		self.eof 		= eof 	# End of file string, appended to report

		self.save_cb 	= save_callback
		self.cbs        = []    # Collect all callback returns

		self.complete   = 0     # between 0 and 1, =n_call/max_n
		self.start_time = None  # start of simulation datetime object
		self.runtimes 	= []    # list of runtimes to calculate some stats
		self.stopped 	= False # Whether the simulation was aborted
		self.stop_time 	= None  # time of stopping of simulation

		# Holds the cumulative profile information of all calls
		self.profile 	= defaultdict(float)

		# Create a counter for each status
		self.counters = OrderedDict()
		for status in self.statuses:
			self.counters[status] 	= 0

		log.debug("Reporter initialised.")

	def __call__(self, status, cb):
		''' Forwards the argument from the callback to the dump_func via a parse_func.'''
		_t0 	= dati.now()
		log.debug("Reporter called. Status: %s. Callback: %s", status, cb)

		# Increment counter calls. Counts calls, not successful calls!
		self.counters['calls'] += self.stepsize

		# Update start time
		if (self.start_time is None) and (self.counters['calls'] == 0):
			# First call. Save start time.
			self.start_time = dati.now()

		# Differentiate between different statusses ...........................
		if status == 'error' or not isinstance(cb, dict):
			# Increment counter
			self.counters['failed'] += self.stepsize

			# Callback is error message
			self.eof='LAST ERROR MESSAGE\n{:}\n{:}'.format('='*18 , cb)

		elif status in self.statuses:
			# Increment counter
			self.counters[status] += self.stepsize

		else:
			raise ValueError("Unknown status {}\nTo add statuses besides {}, call the reporter with argument add_status (list of str).", status, self.statuses)

		# Profiling ...........................................................
		# There is the possibility to pass profiling information to the reporter, if the class has a method get_profile
		if hasattr(cb, 'get_profile') and callable(getattr(cb, 'get_profile')):
			self.counters[status] += self.stepsize

			# Get the profile dictionary
			profile 	= getattr(cb, 'get_profile').__call__()

		elif isinstance(cb, dict) and 'profile' in cb:
			profile 	= cb['profile']

		else:
			# no profiling info passed
			profile 	= None

		if profile:
			# Evaluate the profile
			total_time 	= 0.
			for key, val in profile.items():
				self.profile[key] += val
				total_time += val

		# Save information about the callback .................................
		cbd	= dict(status=status, time=dati.now())

		# Add some more information, if it is present
		cbd['runtime'] 	= total_time if profile else 0.
		cbd['name'] 	= "(no name)"
		if isinstance(cb, dict):
			cbd['name'] 		= cb.get('name', "(no name)")
			cbd['status_msg'] 	= cb.get('status_msg')

		# Save the callback under a separate key, if selected to do so
		if self.save_cb:
			cbd['cb'] 	= cb

		self.cbs.append(cbd)
		self.runtimes.append(cbd['runtime'])

		# Update reporter to be in current state ..............................
		self.update()

		# Shared output behaviour for all .....................................
		log.debug("Reporting ... term_reps: %s,  stdout_only: %s", self.term_reps, self.stdout_only)
		# One line terminal report
		if self.term_reps:
			if status != 'error':
				# Regular terminal report
				self.one_line_terminal_report(print_now=True)
			else:
				# There was an error, also print the message to the terminal
				self.one_line_terminal_report(print_now=True, err_msg=cb)

		# Parse output and save to file
		if not self.stdout_only:
			txt_dump(self.parse(), filepath=self.filepath)

		# On the last call and if configured: send an email
		if (self.send_mail and self.complete == 1.0):
			mail_kwargs 	= self.mail_kwargs
			if self.counters['calls'] >= mail_kwargs.get('min_num_calls', 0):
				to_address 	= mail_kwargs.get('to_address')
				if to_address:
					send_mail(subject="Simulation finished",
							  body_text=self.parse(underline=False,
												   min_lines=1),
							  to_address=to_address)
				else:
					log.warning("No to_address supplied in mail_kwargs! Not sending mail.")
			else:
				log.debug("Minimum number of calls for sending mail not reached; not sending.")

		# Add the time it took for the reporting to the profile
		self.profile['reporting'] += (dati.now() - _t0).total_seconds()

		return

	def update(self) -> None:
		''' Call this before calling any of the get_ methods below! This makes sure that all attributes are up to date.'''

		# Calculate the complete variable
		if self.max_n is not None:
			self.complete = self.counters['calls']/self.max_n
		else:
			raise ValueError('No max_n passed on init of Reporter. ETA cannot be calculated.')

	# Setters . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	def set_max_n(self, max_n):
		''' Sets the maximum number of steps.'''
		self.max_n 	= max_n - len(self.skipping)

	def set_start(self, start_time=None, end="", term_rep: bool=None):
		''' Saves the time of start to the attribute.'''
		if start_time is not None:
			self.start_time = start_time
		else:
			self.start_time = dati.now()

		# Also run the initial terminal report and create the report file
		# Let term_rep argument take priority over term_reps attribute
		if (term_rep is None and self.term_reps) or term_rep:
			self.one_line_terminal_report(print_now=True, end=end)

		# Call report function with parsed input
		if not self.stdout_only:
			txt_dump(self.parse(), filepath=self.filepath)

		return self.start_time

	def set_stopped(self, stopped) -> None:
		''' Sets the simulation status to stopped -- cannot be switched back'''
		if stopped:
			self.stopped 	= True
			self.stop_time 	= dati.now()

	# Getters . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	def was_stopped(self) -> bool:
		''' Returns whether the simulation was stopped before the end.'''
		return self.stopped

	def get_progress(self, offset=(0,0)) -> tuple:
		''' Returns (n_call, max_n)'''
		return (self.counters['calls']+offset[0], self.max_n+offset[1])

	def get_success(self, offset=(0,0)) -> tuple:
		''' Returns (n_call, max_n)'''
		return (self.counters['success']+offset[0], self.max_n+offset[1])

	def get_counters(self) -> OrderedDict:
		''' Returns the counter values'''
		return self.counters

	def get_num_calls_for_time_calc(self):
		''' Returns the number of counts for calls that are used for time calculations.'''
		n 	= 0
		for name, val in self.counters.items():
			if name not in self.ETA_EXCLUDE:
				n 	+= val

		return n

	def get_elapsed(self, as_total_seconds=True) -> float:
		''' Returns elapsed time in seconds'''
		elapsed = dati.now() - self.start_time
		if as_total_seconds:
			return elapsed.total_seconds()
		else:
			return elapsed

	def get_est_time_remaining(self, as_total_seconds=True):
		''' Returns the ETA in seconds of the whole report progress, not taking into account only successful calls.'''

		num_calls_for_time_calc = self.get_num_calls_for_time_calc()

		if num_calls_for_time_calc > 0:
			calls_remaining 	= self.max_n - self.counters['calls']

			return (calls_remaining / num_calls_for_time_calc) * self.get_elapsed(as_total_seconds=as_total_seconds)
		else:
			return None

	def get_eta(self):
		''' Get estimated time of end of simulation, returns datetime object'''
		est_time_remaining 	= self.get_est_time_remaining()

		if est_time_remaining is not None:
			return dati.now() + timedelta(0, est_time_remaining)
		else:
			return None

	def get_runtime_stats(self) -> list:
		'''Return some runtime statistics.'''

		if len(self.runtimes) <= 5:
			# Only calculate if there is enough data
			return None
		else:
			rts = self.runtimes
			d = OrderedDict()

		d['mean'] 			= np.nanmean(rts)
		d[' (last 50%)']	= np.nanmean(rts[-len(rts)//2:])
		d[' (last 20%)']	= np.nanmean(rts[-len(rts)//5:])
		d[' (last 5%)'] 	= np.nanmean(rts[-len(rts)//20:])
		d['std'] 			= np.nanstd(rts)
		d['min'] 			= np.nanmin(rts)
		d['at 25%'] 		= np.percentile(rts, 25)
		d['median'] 		= np.nanmedian(rts)
		d['at 75%'] 		= np.percentile(rts, 75)
		d['max'] 			= np.nanmax(rts)

		return d

	def get_callbacks(self) -> list:
		return self.cbs

	def get_last_callback(self):
		''' Returns the latest callback summary.'''
		if self.save_cb:
			return self.cbs[-1]
		else:
			return None

	def skip(self):
		''' Called when a simulation is skipped.'''
		raise NotImplementedError("Skip method not implemented yet.")

	# Output-related ..........................................................

	def one_line_terminal_report(self, print_now: bool=False, offset: tuple=(0,0), err_msg: str=None, end: str="", if_enabled: bool=False) -> str:
		''' Prints a one-line terminal report. For offset=(1,0) the method should be called to indicate the _current_ simulation, for (0,0) the _finished_ simulation. When an error message is passed, this will also be displayed.'''

		if if_enabled:
			# only do the report, if the attribute term_reps is True
			if not self.term_reps:
				return ""

		clear_line()

		progress = self.get_progress(offset=offset)
		s        = colored("\r--- {1:>{0:d}d}/{2:<{0:d}d} --- ".format(len(str(progress[1])),*progress), attrs=['bold'])

		if self.stopped:
			s += colored("(Stopped)", 'red', attrs=['bold'])

		if self.bell:
			s += "\a"

		if err_msg is not None:
			s += "\n{bell:}{msg:}".format(bell="\a\a" if self.bell else "",
										  msg=err_msg)

		if progress[0] == progress[1]:
			s += "\n"

		if print_now:
			print(s, end=end)
			sys.stdout.flush()
		else:
			return s

	def parse(self, min_lines: int=52, underline: bool=True) -> str:
		''' More advanced version of the above parser, also implementing a progress bar and timing stuff as well as number of flagged simulations.'''

		log.debug("Parsing reporter state ...")

		# Get some values and set default values ..............................
		# Get progress
		progress 		= self.get_progress() # returns [n_calls, max_n]

		# Get counters: calls, done, aborted, failed, flagged
		counters 		= self.get_counters()
		n_calls			= counters['calls']
		n_remaining 	= progress[1] - n_calls
		n_success		= counters['success']
		n_skipped		= counters['skipped']
		n_fail 			= counters['failed'] + counters['aborted']
		n_time_calc 	= self.get_num_calls_for_time_calc()

		# Settings, timings, time format...
		timestrf 		= '%d.%m.%Y,  %H:%M:%S'	# format str for datetime obj.
		start_time 		= self.start_time
		now      		= dati.now()
		elapsed  		= self.get_elapsed()
		est_remaining	= self.get_est_time_remaining() # as seconds
		eta 			= self.get_eta()	# datetime object
		rtstats 		= self.get_runtime_stats()

		# Some default values
		time_per_sim 	= None
		eta_str 		= "?" 		# ETA
		tps_str 		= "?" 		# time per sim
		est_rm_str		= "?" 		# est. remaining
		obs_dir_size 	= None
		num_items 		= None
		status_file_report = None

		# Calculations ........................................................
		# Affected by initial call (progress 0)
		if n_time_calc > 0:
			time_per_sim 	= elapsed / n_time_calc

		# Convert some values to strings
		if eta is not None:
			eta_str 		= eta.strftime(timestrf)

		if est_remaining is not None:
			est_rm_str		= format_time(est_remaining)

		if time_per_sim is not None:
			tps_str 		= format_time(time_per_sim)

		# Status files
		if self.status_files:
			status_file_report = _get_status_files(self.obs_dir)

		# Calculate some values to be displayed at the end
		if self.complete == 1 and self.obs_dir is not None:
			# Determining folder size takes quite long, only do this once.
			obs_dir_size 	= str(subprocess.check_output("du -L -sh -c "+self.obs_dir+" | tail -n 1", shell=True).decode('utf-8')).replace("total", "").replace(" ", "").replace("\t\n","")

			# Determine number of files
			try:
				num_items = len(glob.glob(self.obs_dir+"**", recursive=True))
			except Exception as err:
				num_items = None
				log.debug("Tried to determine number of items, but got an error:  %s", err)

		# For progress bar  . . . . . . . . . . . . . . . . . . . . . . . . .
		# Symbols
		pb_width 		= 50 	# width
		tick_sym 		= "#"	# symbol for successful calls
		add_sym 		= "*"	# symbol for additional statuses
		skip_sym 		= "-"	# for skipped
		fail_sym 		= "x"	# for failed and aborted
		space_sym 		= fail_sym if self.was_stopped() else " " # for space

		# Calculate number of ticks
		if n_calls > 0:
			pb_ticks = int(round(self.complete*(n_success/n_calls)*pb_width))
			pb_add 	 = int(round(self.complete*(
								  (n_time_calc-n_success)/n_calls)*pb_width))
			pb_fails = int(round(self.complete*(n_fail/n_calls)*pb_width))
			pb_skips = int(round(self.complete*(n_skipped/n_calls)*pb_width))
		else:
			pb_ticks, pb_add, pb_fails, pb_skips = 0, 0, 0, 0

		# number of space ticks:
		pb_space 	= pb_width - (pb_ticks + pb_add + pb_fails + pb_skips)


		# ---------------------------------------------------------------------
		# Start building the string -------------------------------------------
		s =  "\nP R O G R E S S   R E P O R T\n"\
			 +(("="*29)+"\n")*underline + "\n"

		# Progress bar ........................................................
		s += "-- {1:>{0:d}d}/{2:<{0:d}d} -- ".format(len(str(progress[1])),
													   *progress)
		s += "[{:}{:}{:}{:}{:}] {:>5.1f}%\n\n".format(	tick_sym*pb_ticks,
														add_sym*pb_add,
														skip_sym*pb_skips,
														fail_sym*pb_fails,
														space_sym*pb_space,
														self.complete*100)

		# Timing ..............................................................
		if self.complete < 1.0:
			s += 	"  Started:      {:}\n" \
					"  Last call:    {:}\n" \
					"  Elapsed:      {:}\n" \
					"  Per sim.:     {:}\n" \
					"  Est. left:    {:}\n" \
					"  Est. end:     {:}\n" \
					"".format(start_time.strftime(timestrf),
					          now.strftime(timestrf), format_time(elapsed),
					          tps_str, est_rm_str, eta_str)

		else:
			s += 	"  Started:      {:}\n" \
					"  Elapsed:      {:}\n" \
					"  Per sim.:     {:}\n" \
					"  Finished:     {:}\n" \
					"".format(start_time.strftime(timestrf),
					          format_time(elapsed), tps_str,
					          now.strftime(timestrf))

		# To both: time it was stopped
		if self.was_stopped():
			# Add stopped time from attribute
			s +="  Stopped:      {:}\n".format(self.stop_time.strftime(timestrf))

		# Number of files and directory size
		if num_items is not None:
			s += "\n  Num. items:   {:}\n".format(num_items)

		if obs_dir_size is not None:
			s += "  Dir. size:    {:}\n".format(obs_dir_size)

		# Counters and status .................................................
		s += "\n\nC O U N T E R S\n"+(("-"*15)+"\n")*underline

		# Display counters
		_digits = len(str(max(n_remaining, *list(counters.values()))))
		for key, val in counters.items():
			s += "  {v:>{d:}} {k:}\n".format(k=key, v=val, d=_digits)

		# Remaining
		s += "\n  {v:>{d:}} remaining\n".format(v=n_remaining, d=_digits)

		# Statistics ..........................................................
		if rtstats:
			s += "\n\nR U N T I M E   S T A T I S T I C S\n"+(("-"*35)+"\n")*underline

			s+= "\n".join(["  {k:<13s} {v:}".format(k=k,
			                                        v=format_time(v, ms_precision=1))
			               for k, v in rtstats.items()]) + "\n"

		# Status of simulations
		if n_calls > 0 or status_file_report:
			s += "\n\nF I N I S H E D   S I M U L A T I O N S\n"\
				 +(("-"*39)+"\n")*underline

			s += self.parse_last_cbs()

			if status_file_report is not None:
				s += "\n"
				# Display status file report
				for status, sim_names in status_file_report.items():
					s += "{:<14}\t{:}\n".format(status.title()+":", ", ".join(sorted(sim_names)))


		# Profiling and EOF string  ...........................................
		prof_str = self.parse_profile()
		if len(prof_str):
			s += "\n\nP R O F I L I N G   S U M M A R Y\n"\
				 +(("-"*33)+"\n")*underline + prof_str

		# End of file string
		if len(self.eof):
			s += "\n\n"+str(self.eof)

		if s.count("\n") < min_lines:
			s += "\n"*(min_lines - s.count("\n"))

		return s

	def parse_last_cbs(self, num: int=10, timestrf="%H:%M:%S", multiline: bool=False) -> str:
		''' Parses the last num callbacks and returns a string with the finish time and exit status.'''

		num = min(self.counters['calls'], num)
		cbs = self.get_callbacks()[-num:]

		max_len_names 	= max(6, max([len(cb.get('name')) for cb in cbs]))

		# Format strings
		SF 	= "  {t:8}  {n:<{w:d}}  {rt:>7}  {s:<14.14}   {m:}\n"
		SF2 = "{:}… {{msg:}}\n".format(" "*(2+8+2+max_len_names+2+7+2+14+3))

		# Header row
		_s 	= SF.format(t='time', n='name', w=max_len_names,
						rt='runtime', s='status', m='message')

		for cb in reversed(cbs):
			# Get information
			time 	= cb.get('time').strftime(timestrf)
			name 	= cb.get('name')
			rtime	= cb.get('runtime', 0)
			status 	= cb.get('status')
			msg 	= cb.get('status_msg', '')
			as_ml	= False

			# Format runtime
			rts = format_time(rtime, max_num_elements=2) if rtime > 0. else ""

			# Truncate message if too long
			if len(msg) > 32:
				if multiline:
					as_ml	= True
					msgs 	= [msg[i:i+31] for i in range(0, len(msg), 31)]
					msg 	= msgs[0] + " …"
				else:
					# Truncate the message
					msg 	= msg[:32] + "…"

			# Build and append string
			_s 	+= SF.format(t=time, n=name, w=max_len_names,
							 rt=rts, s=status, m=msg)

			if as_ml:
				for msg in msgs[1:]:
					_s 	+= SF2.format(msg=msg)

		return _s

	def parse_profile(self, min_num: int=5, max_num: int=25, lim: float=0.001) -> str:
		''' Creates a nice looking attribute profile with runtimes and percentages. The lim argument specifies under which percentage value to stop listing...'''

		# Sum up total time (in seconds)
		t_tot = 0
		for _, val in self.profile.items():
			t_tot += val

		# Sort list and only keep the first num entries
		prof_sorted = sorted(self.profile.items(),
							 key=lambda tup: tup[1], reverse=True)
		prof_len 	= len(prof_sorted)

		if prof_len == 0:
			return ""

		# Create string and return
		prof_str 	= ""
		i 			= 0
		for name, time in prof_sorted:
			percent = time/t_tot*100

			# Decide whether to show this line or stop altogether
			if ((i >= min_num and percent < lim*100)
				or (i >= max_num and i <= prof_len-1)):
				if percent < lim*100:
					last_percent = lim*100 # reached the set limit
				else:

					last_percent = percent # this was the effective limit

				# Add to string
				prof_str += "\n({:} others with <= {:.2f}% contribution)\n".format(prof_len - i, last_percent)

				break
			else:
				# do not break -> show this line and increment the counter
				i += 1

			# Add to string
			prof_str += "{:<20}  {:>4.1f}%  {:>14}\n".format(name, percent,format_time(float(time), ms_precision=1))

		prof_str += "\nTotal profiled CPU time: {:} ({:d}% of elapsed time)\n".format(format_time(float(t_tot), ms_precision=1), int(t_tot/self.get_elapsed()*100))

		return prof_str


# -----------------------------------------------------------------------------
# Helpers .....................................................................
def txt_dump(s, filepath, mode='w'):
	''' Writes a string to a .txt file. Filepath is without file extension. File mode can be specified.'''
	with open(filepath+'.txt', mode) as f:
		f.write(s)

def clear_line(only_in_tty=True, break_if_not_tty=True):
	''' Clears the current terminal line and resets the cursor to the first position using a POSIX command.'''
	# http://stackoverflow.com/questions/5419389/how-to-overwrite-the-previous-print-to-stdout-in-python
	if break_if_not_tty or only_in_tty:
		# check if there is a tty
		is_tty = sys.stdout.isatty()

	# Differentiate cases
	if (only_in_tty and is_tty) or not only_in_tty:
		# Print the POSIX character
		print('\x1b[2K\r', end='')

	if break_if_not_tty and not is_tty:
		# print linebreak (no flush)
		print('\n', end='')

	# no linebreak, flush manually
	sys.stdout.flush()

def format_time(time_in_s, ms_precision=1, max_num_elements=4):
	''' Helper function to format time in seonds. If ms_precision > 0, decimal places will be shown'''

	if not isinstance(ms_precision, int):
		raise TypeError('Argument ms_precision was expected to be int, was '+str(type(ms_precision)))

	divisors    = [24*60*60, 60*60, 60, 1]
	letters     = ['d', 'h', 'm', 's']
	remaining   = float(time_in_s)
	elements 	= []

	for divisor, letter in zip(divisors, letters):
		# Break if the maximum number of elements is not yet reached
		if len(elements) >= max_num_elements:
			break

		time_to_represent   = int(remaining/divisor) # flooring
		remaining           -= time_to_represent * divisor

		if time_to_represent > 0 or elements:
			# Distinguish between seconds and other divisors for short times
			if ms_precision <= 0 or time_in_s > 60:
				# Regular behaviour: Seconds do not have decimals
				elements.append('{:d}{:}'.format(time_to_represent, letter))

			elif ms_precision > 0 and letter == 's':
				# Decimal places for seconds
				elements.append('{val:.{prec:d}f}s'.format(val=time_to_represent+remaining, prec=int(ms_precision)))

	if not elements:
		# No elements added yet -> everything below 1s so far ...
		if ms_precision == 0:
			# Just show an approximation
			elements.append('< 1s')

		elif ms_precision > 0:
			# Show as decimal with ms_precision decimal places
			elements.append('{val:{tot}.{prec}f}s'.format(val=remaining,tot=int(ms_precision)+2, prec=int(ms_precision)))

	return " ".join(elements)

def status_file(path, name, status=None, delete_only=False):
	''' Creates a file of the pattern _name_status in the directory path and deletes any files of the pattern _name_something in this directory before doing so. If delete is True, those files get deleted.
	This method can be used to see what status a multiprocessing worker has...'''

	# Find and delete
	previous_files = glob.glob('{path:}_{name:}_*'.format(path=path,name=name))
	for prev_f in previous_files:
		os.remove(prev_f)

	# Create new file
	if status is not None and not delete_only:
		status_fpath = '{path:}_{name:}_{status:}'.format(path=path,
														  name=name,
														  status=status)

		# If not already exists, create by touching
		if not os.path.isfile(status_fpath):
			touch(status_fpath)

def _get_status_files(path, statusses=None):
	''' Gets all files of the form path/_name_status for status in list statusses'''
	if not statusses:
		statusses =['simulating', 'evaluating', 'plotting', 'error']

	report = OrderedDict()

	for status in statusses:
		sfiles = glob.glob('{path:}_*_{status:}'.format(path=path,
														status=status))

		report[status] = []

		for sfile in sfiles:
			basename 	= os.path.basename(sfile)
			name 		= basename[1:-len(status)-1]

			report[status].append(name)

	return report

# E-Mail related ..............................................................
def send_mail(*args, subject: str, body_text: str, to_address: str, attachment=None):
	''' Method to send an email. A file can also be attached by having keys 'filename' and 'path' in dict attachment.'''

	# Configuration
	smtp_server     = "smtp.gmail.com"
	fromaddr        = "your.personal.notifier@gmail.com"
	pwd             = "asdf1337"
	fromname        = "Rupert the ReportBot" # :)

	# Initialize MIME Multipart Message dict
	msg = MIMEMultipart()

	msg['From']     = formataddr((str(Header(fromname, 'utf-8')), fromaddr))
	msg['To']       = to_address
	msg['Subject']  = subject

	msg.attach(MIMEText(body_text, 'plain'))

	# Handle attachment
	if attachment and 'filename' in attachment and 'path' in attachment:
		filename = attachment['filename']
		attachment = open(attachment['path'], "rb")

		part = MIMEBase('application', 'octet-stream')
		part.set_payload((attachment).read())
		encoders.encode_base64(part)
		part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
		msg.attach(part)

	elif attachment is not None:
		# Print a notification of the failed
		log.warning("Argument 'attachment' was passed, but did not include 'filename' and/or 'path' keys. No file will be attached to the email.\nValue of 'attachment' argument: %s", attachment)

	# Initialize server session
	server  = smtplib.SMTP(smtp_server, 587)
	server.starttls()
	server.login(fromaddr, pwd)
	text    = msg.as_string()

	# Send and quit
	server.sendmail(fromaddr, to_address, text)
	server.quit()

	log.note("Email successfully sent to %s.", to_address)

	return True
