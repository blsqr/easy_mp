#!/usr/bin/env python3

from setuptools import setup

# Dependency lists
install_deps = ['numpy>=1.14', "pygments>=2.1.3", "termcolor>=1.1"]
test_deps    = ['pytest>=3.4.0', 'pytest-cov>=2.5.1']


setup(name='easy_mp',
      version='0.1',
      description='Simplifies python multiprocessing and reporting',
      author='Yunus Sevinchan',
      author_email='Yunus.Sevinchan@iup.uni-heidelberg.de',
      url='https://ts-gitlab.iup.uni-heidelberg.de/yunus/easy_mp',
      license='MIT',
      classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Utilities'
      ],
      packages=['easy_mp'],
      install_requires=install_deps,
      tests_require=test_deps,
      test_suite='py.test',
      extras_require=dict(test_deps=test_deps)
      )
