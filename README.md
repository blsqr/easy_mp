# `easy_mp` — Python multiprocessing, simplified.

This package extends the Python `multiprocessing` package to simplify working with it.

Mainly, it provides the `EasyPool` and `Reporter` classes, which can be used together to perform tasks in parallel and report on their status.

### Note:
This package was developed alongside [`deeevolab`](https://ts-gitlab.iup.uni-heidelberg.de/yunus/deeevolab) and moved out of it at one point, which explains the odd history of this repository.

While functional, it could certainly use some clean up and pythonisation. Furthermore, it might benefit from more generalisation.

If you want to get involved, contact me. :)
