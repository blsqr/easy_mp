''' This package aims to provide an easy environment for multiprocessing and reporting.'''

import os
import sys
import signal
import traceback
import logging
import multiprocessing as mp
from platform import node 	# for hostname

# Set up logging
log = logging.getLogger(__name__)
mp.log_to_stderr()
mp.get_logger().setLevel(logging.WARNING)

# For colored traceback
try:
	from pygments import highlight
	from pygments.lexers import get_lexer_by_name
	from pygments.formatters import Terminal256Formatter
except ImportError:
	PYGMENTS_IMPORTED = False
else:
	PYGMENTS_IMPORTED = True

# For colored terminal commands
def no_color(s, *args, **kwargs):
	''' Just returns the argument s as a string -- used to substitue termcolor.colored if no colored output is desired.'''
	return str(s)

if sys.stdout.isatty():
	try:
		from termcolor import colored
	except ImportError:
		# not installed, use a different method, which does not do anything with the color argument
		colored = no_color
else:
	# just set to a do-nothing-function
	colored = no_color

# Local constants .............................................................
# Colored terminal output:
COLOR_TB 		= True
TF_BG 			= 'dark' 	# terminal formatter background
TF_STYLE 		= 'default' # manni,  igor,  lovelace,  xcode,  vim,  autumn,  vs,  rrt,  native,  perldoc,  borland,  tango,  emacs,  friendly,  monokai,  paraiso-dark,  colorful,  murphy,  bw,  pastie,  algol_nu,  paraiso-light,  trac,  default,  algol,  fruity
TERM_FORMATTER	= Terminal256Formatter(bg=TF_BG, style=TF_STYLE)

# Number of columns of the terminal (e.g. used in printing lines over the whole width)
try:
	_, TERM_COLS 	= os.popen('stty size', 'r').read().split()
except:
	# Probably not run from terminal --> set value manually
	TERM_COLS 		= 72
else:
	TERM_COLS 		= int(TERM_COLS)

###############################################################################

class EasyPool:
	''' Manages a multiprocessing.Pool object. As this is somehow not a class, it does not inherit from multiprocessing.Pool, but wraps the important methods.
	'''

	def __init__(self, *args, num_processes: int='auto', process_kwargs: dict=None, force_pool: bool=False, reporter=None, stopdir: str=None, exc_behaviour: int=1, niceness=0, **pool_kwargs):
		''' TODO write.'''

		if len(args) > 0:
			raise ValueError("EasyPool takes no positional arguments, but got {}. Pass arguments only via keywords!".format(args))

		# Initialise attributes
		self.reporter 	= reporter
		self.niceness 	= niceness
		self.wrk_cnt 	= 0

		# Determine number of processes for the pool
		if num_processes == 'auto':
			# Use the defaults
			log.debug("Calculating number of processes using kwargs: %s", process_kwargs)

			process_kwargs= {} if process_kwargs is None else process_kwargs
			num_processes = _calc_num_processes(**process_kwargs)

		elif isinstance(num_processes, int) and num_processes >= 1:
			# use the given number, nothing to do
			log.debug("Using the argument value for setting the number of processes.")
		else:
			raise ValueError("Argument 'num_processes' needs to be 'auto' or a positive int, was {}, type {}".format(num_processes, type(num_processes)))

		# If only one process, do not use the pool.
		self.mp_active 	= bool(num_processes > 1) or force_pool

		if self.mp_active:
			if 'processes' in pool_kwargs:
				pool_kwargs.pop('processes')
				log.warning("Passed argument 'processes' as additional keyword argument; ignoring this argument, because the number of processes for the pool should be set using the 'num_processes' keyword argument of EasyPool.")

			# Initialize pool
			self._pool 	= mp.Pool(processes=num_processes, **pool_kwargs)

			# Save number processes
			self.num_processes = num_processes

		else:
			log.info("EasyPool running in Single Process Mode ...")
			self._pool 	= None

			# Save number processes
			self.num_processes = 1

		# Initialise callback handler
		self.cbhandler 	= CallbackHandler(pool=self._pool,
		                                  reporter=self.reporter,
		                                  mp_active=self.mp_active,
		                                  exc_behaviour=exc_behaviour,
		                                  stopdir=stopdir)

		log.info("%s (%d processes) and CallbackHandler initialised.",
		         self.__class__.__name__, num_processes)

		return

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def get_num_processes(self):
		return self.num_processes

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def apply(self, func, *func_args, niceness: int=None, suppress_result: bool=False, **func_kwargs):
		''' A wrapper around the pool's apply_async method, which includes the ability to '''

		if niceness is None:
			niceness 	= self.niceness

		if self.mp_active:
			log.debug("Adding asynchronous worker ...")
			res = self._pool.apply_async(func_wrapper,
			                             (func, niceness, suppress_result,
			                              func_args, func_kwargs),
			                             callback=self.cbhandler,
			                             error_callback=self.cbhandler)
			self.wrk_cnt 	+= 1
			log.debug("Worker added. (Total workers: %d)", self.wrk_cnt)
			return res

		else:
			# Running in single process mode, i.e.: execute sequentially
			self.wrk_cnt += 1
			log.debug("Starting sequential worker %d ...", self.wrk_cnt)
			cb 	= func_wrapper(func, niceness, suppress_result,
			                   func_args, func_kwargs)
			self.cbhandler(cb)
			log.debug("Worker %d finished, callback handler was invoked.", self.wrk_cnt)
			return cb

	def close_and_run(self, ignore_kbint: bool=False):
		''' A combination of pool.close and pool.join -- this will let the workers start working and is thus necessary to call!'''

		self.close()

		try:
			self.join()

		except KeyboardInterrupt:
			if ignore_kbint:
				print("")
				log.warning("KeyboardInterrupt! Stopping workers ...")
				self.terminate()
			self.join()

	def close(self, *args, **kwargs):
		''' Close the pool, meaning: there are no more jobs to be sent to workers.'''
		if self.mp_active:
			self._pool.close(*args, **kwargs)
			log.debug("Closed EasyPool; no new workers accepted from now on.")

		else:
			log.debug("Called close() on EasyPool in single process mode; this has no effect.")

		return

	def join(self, *args, **kwargs):
		''' Close the pool, meaning: there are no more jobs to be sent to workers.'''
		if self.mp_active:
			log.debug("Waiting to join %d workers ...", self.wrk_cnt)
			self._pool.join(*args, **kwargs)
			log.debug("Joined EasyPool with a total of %d workers.",
			          self.wrk_cnt)

		else:
			log.debug("Called join() on EasyPool in single process mode; this has no effect.")

		return

	def terminate(self, *args, **kwargs):
		''' Call the terminate method of the pool'''
		log.warning("Terminating EasyPool with %d workers ...", self.wrk_cnt)
		return self._pool.terminate(*args, **kwargs)

	def send_callback(self, *args):
		''' Manually call the callback handler with the given arguments.'''
		self.cbhandler(*args)

###############################################################################

class CallbackHandler:
	''' Handles the callback and exceptions raised in the multiprocessing worker. Furthermore, it can pass the callback on to a reporter object.'''

	# Codes to which to respond
	ABORT_CODES 	= ['abort', 'stop', 'stopped']
	SKIP_CODES 		= ['skipped', 'skip']

	def __init__(self, *args, pool=None, reporter=None, mp_active: bool=True, exc_behaviour: int=1, stopdir: str=None):
		''' Initialise the CallbackHandler object.'''

		if len(args):
			raise ValueError("CallbackHandler initialisation does not take any positional arguments, but got {}.\nPass via keyword arguments only!".format(args))

		# Save attributes
		self.reporter 		= reporter		# the reporter object to redirect the callback to
		self.pool 			= pool 			# The mp.Pool

		self.mp_active 		= mp_active		# whether multiprocessing is active
		self.exc_behaviour	= exc_behaviour	# behaviour for exceptions

		self.stopdir 		= stopdir		# the directory of the stop-file
		self.stoppable	 	= isinstance(stopdir, str)# whether it's stoppable
		self.stopped 		= False 		# Whether the run was stopped

		log.debug("CallbackHandler initialised with arguments:\n\tmp_active: %s,  exc_behaviour: %d\n\tstoppable: %s,  stopdir: %s", self.mp_active, self.exc_behaviour, self.stoppable, self.stopdir)

	def __call__(self, cb):
		''' This is the main method of this class.

		In the usual case, it is called as a callback from another method and gets one argument. Depending on the form of this argument, this might mean several different things.

		It checks for any exceptions and then passes the callback on to the reporter object (if given).'''

		log.debug("Got callback: %s", cb)

		# Catch errors ........................................................
		# In case of an error, the callback has the form (RuntimeError(), msg)
		if (isinstance(cb, tuple) and len(cb) == 2
		    and isinstance(cb[0], RuntimeError)):
			# There was an error.

			self._handle_exc_cb(cb)

			# Return here, because the program might not get through to the end due to sys.exit() and the calls to the reporter have to be made prior to this... (see self.exc_behaviour < 2 above)
			return

		# Not an error. Check, if the callback requests abort, or just skips
		if cb in self.ABORT_CODES:
			# Create _stop file, then update the report and return
			status 	= 'aborted'

			if not self.stopped:
				if self.stopdir and self.stoppable:
					touch(self.stopdir + '_stop')
					self.stopped = True

					# Tell reporter
					if self.reporter:
						self.reporter.set_stopped(True)

				else:
					log.warning("Got STOP signal, but the simulation is not stoppable or no stop directory was set.\nstopdir: %s, stoppable: %s", self.stopdir, self.stoppable)

		elif cb in self.SKIP_CODES:
			# Not an error or abort, but a planned skip
			status 	= 'skipped'

		else:
			# Not an error and no abort --> normal case
			if isinstance(cb, dict) and 'status' in cb:
				# Callback dictionary passed
				status 	= cb.get('status')
			else:
				# No callback dictionary passed
				status 	= 'success'

		# Pass on to reporter, if present
		if self.reporter:
			self.reporter(status, cb)

		# Return the callback, if any other method wants to handle it
		return cb

	def _handle_exc_cb(self, cb): # TODO include _handle_exc here?!
		''' Handles a callback that signalled an exception.

		In this case, the callback is of form (err, msg) (from _handle_exc)
		'''

		err, msg	= cb
		add_s 		= '' # additional string to append

		# Distinguish between running in parallel and running in one proces
		if self.mp_active:
			add_s += 'Error occured in multiprocessing worker.'
		else:
			add_s += 'Error occured in the single process.'

		# Build error message depending on whether the simulation can be stopped or not ....................................................
		if self.exc_behaviour < 1:
			if self.mp_active:
				# Initiate stopping of other mp workers, by writing stop file to simulation directory
				if self.pool is not None:
					add_s += '\nTermination of pool initiated.'
				elif self.stopdir is not None and self.stoppable:
					add_s += '\n(Gentle) stopping of other processes initiated.'
				else:
					add_s += '\nOther processes could not be stopped; processes need to be terminated manually.'

			else:
				add_s += '\nStopping performed by calling sys.exit().'

		else:
			add_s += '\nWas ignored. Execution will continue.'

		# Indicate exception behaviour
		add_s 	+= '\n(Exception behaviour level: {})'.format(self.exc_behaviour)

		# Build error message
		err_msg = "\n\n{div:}\n{msg:}\n{add_s:}\n{div:}\n".format(msg=msg, add_s=add_s, div=colored('*'*TERM_COLS, 'red', attrs=['bold']))

		# Preparations to be done before reporting ........................
		# if self.exc_behaviour < 3:
		# 	# do things here, if desired
		# 	pass

		# if self.exc_behaviour < 2:
		# 	# do things here, if desired
		# 	pass

		if self.exc_behaviour < 1:
			if self.mp_active:
				if self.stopdir is not None and self.stoppable and not self.stopped or self.pool is not None:

					# Create stop file and change flag, if not terminating via the pool
					if self.pool is None:
						touch(self.stopdir + '_stop')

					# Set flag
					self.stopped 	= True

					# Also tell reporter
					if self.reporter:
						self.reporter.set_stopped(True)
				else:
					# was already stopped by another worker or cannot be stopped
					pass

		# Different raising and printing behaviour for exceptions..........
		# # Doing nothing
		# if self.exc_behaviour < 3:
		# 	pass

		# Printing
		if self.exc_behaviour < 2:
			if self.reporter is not None:
				self.reporter('error', err_msg)

		# Exiting
		if self.exc_behaviour < 1:
			if self.mp_active:
				# if the multiprocessing pool was passed, it is possible to exit here
				if self.pool:
					self.pool.terminate()

			else:
				# can be simply stopped by calling sys.exit()
				sys.exit()

		return


###############################################################################
# Helpers

def func_wrapper(func, niceness: int, suppress_result: bool, func_args: list, func_kwargs: dict):
	''' This wraps the function func, which will be executed by the worker, and is thus the layer between the multiprocessing pool and the executed method.

	When called from EasyPool, the return value of this wrapper is the argument of the callback method, i.e. CallbackHandler(returnVal) gets called at the exit of this wrapper method.

	Note: This method is only entered once the Pool decides to let this particular worker start working on func. Thus, any code here is not executed at the time apply_async is executed, but at the time the worker will start working.
	'''

	# Set the worker up such that it ignores the interrupt signal
	# signal.signal(signal.SIGINT, signal.SIG_IGN)

	# TODO build stopping/skipping of simulations here (might need more arguments) ... something like:
	# if os.path.isfile(sim_dir+'_stop'):
	# 	return 'stopped' # this will tell the CallbackHandler, that the simulation run was stopped

	# Set the niceness, if positive and not already done so
	if int(niceness) > 0:
		current_niceness 	= os.nice(0)
		if current_niceness < niceness:
			new_niceness 	= int(niceness) - current_niceness
			os.nice(new_niceness)
			log.debug("Set processes niceness from %d to %d.",
			          current_niceness, new_niceness)

	# Run the actual simulation method and catch any error.
	try:
		res = func(*func_args, **func_kwargs)

	except Exception as exc:
		# analyse traceback and pass error on to the callback function
		msg = "An exception was raised in method '{}':".format(func.__name__)
		return _handle_exc(exc, msg=msg, tb=traceback.format_exc())

	else:
		if suppress_result:
			log.debug("Suppressing function result ...")
			return None

			# TODO Could implement a test for whether return will fail or not

		# no problem with returning the result, because not outside of main thread
		return res

# TODO combine with similar method in CallbackHandler?
def _handle_exc(exc, msg: str, tb="", color_tb=COLOR_TB):
	''' Method used to handle exceptions occuring in mp_wrapper.

	Note that any error is converted to a RuntimeError, but the initial error is preserved in the message argument.
	Note furthermore, that the CallbackHandler recognizes errors by a tuple (Exception, msg) being passed.
	'''

	if color_tb and PYGMENTS_IMPORTED:
		lexer 		= get_lexer_by_name("pytb", stripall=True)
		#formatter 	= TerminalFormatter(bg=TF_BG, linenos=TF_LINENOS)
		formatter 	= TERM_FORMATTER
		tb 			= highlight(''.join(tb), lexer, formatter)

	s 	= colored("Exception caught.\n", 'red', attrs=['bold'])
	s 	+= "{msg:} {exc:}\n".format(msg=msg, exc=exc)
	s 	+= "\n{tb:}".format(tb=tb) if tb != "" else ""

	# returns a tuple (Exception, msg)
	return RuntimeError(), s

def _calc_num_processes(max_num_processes=32, extra_main_thread=True, max_num_local_processes=None, local_hostname=None):
	''' Calculates the number of processes to use in a multiprocessing pool, taking into account the number of available cores and whether this script is executed on a specific ('local') host.'''

	# Determine number of processes depending on cpu_count
	num_processes = min(max_num_processes,
	                    mp.cpu_count() - int(extra_main_thread))

	# For one particular hostname, choose different behaviour:
	if local_hostname and max_num_local_processes:
		this_hostname = node()

		if local_hostname.lower() == this_hostname.lower():
			num_processes = min(max_num_local_processes,
								mp.cpu_count()-int(extra_main_thread))

			log.debug("Running on local host ('%s'). Number of processes set to %d (max: %d)...", this_hostname, num_processes, max_num_local_processes)

		else:
			log.debug("Running on remote host ('%s'). Number of processes set to %d (max: %d)...", this_hostname, num_processes, max_num_processes)

	return num_processes

def touch(path):
	''' Like the unixoid touch command.'''
	with open(path, 'a'):
		os.utime(path, None)

